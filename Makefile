AUTOMATOR_SCRIPT_PATH:="/System/Library/Automator/Combine PDF Pages.action/Contents/Resources/join.py"

.PHONY: all
all: Yuteng_ZHONG_CV_ENG.pdf Yuteng_ZHONG_CV_CHN.pdf Yuteng_ZHONG_CV.pdf

Yuteng_ZHONG_CV.pdf: Yuteng_ZHONG_CV_ENG.pdf Yuteng_ZHONG_CV_CHN.pdf
	${AUTOMATOR_SCRIPT_PATH} -o $@ $^

Yuteng_ZHONG_CV_ENG.pdf: Yuteng_ZHONG_CV_ENG.tex
	latexmk -xelatex -silent Yuteng_ZHONG_CV_ENG

Yuteng_ZHONG_CV_CHN.pdf: Yuteng_ZHONG_CV_CHN.tex
	latexmk -xelatex -silent Yuteng_ZHONG_CV_CHN

.PHONY: clean
clean:
	latexmk -c

.PHONY: clean-all
clean-all:
	latexmk -C
	rm -rf Yuteng_ZHONG_CV.pdf
