# Curriculum Vitae

I'm Yuteng ZHONG who is passionate in open-sources technologies.

## Usage

```bash
# Make all pdfs
$ make

# Only the ENG version
$ make Yuteng_ZHONG_CV_ENG.pdf

# Only the CHN version
$ make Yuteng_ZHONG_CV_CHN.pdf

# Clean
$ make clean

# Clean all (including the *.pdf)
$ make clean-all
```

Prebuilt PDFs:

* Merged: [Yuteng_ZHONG_CV.pdf](https://raw.githubusercontent.com/zonyitoo/cv/master/Yuteng_ZHONG_CV.pdf)
* ENG Only: [Yuteng_ZHONG_CV_ENG.pdf](https://raw.githubusercontent.com/zonyitoo/cv/master/Yuteng_ZHONG_CV_ENG.pdf)
* CHN Only: [Yuteng_ZHONG_CV_CHN.pdf](https://raw.githubusercontent.com/zonyitoo/cv/master/Yuteng_ZHONG_CV_CHN.pdf)

## Licenses

### Template: moderncv.cls

```latex
%% start of file `moderncv.cls'.
%% Copyright 2006-2012 Xavier Danaux (xdanaux@gmail.com).
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License version 1.3c,
% available at http://www.latex-project.org/lppl/.
```
